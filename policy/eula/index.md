---
title: Kali Linux EULA
description:
icon:
type: post
weight:
author: ["g0tmi1k",]
---

Kali Linux's End-User License Agreement (EULA), can be found at the following URL: [kali.org/docs/policy/EULA.txt](/docs/policy/EULA.txt).
